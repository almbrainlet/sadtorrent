// ==UserScript==
// @name        Sadpanda Torrent Inline + eze info.json
// @version     0.9.10
// @author      almbrainlet
// @namespace   almbrainletSadtorrent
// @homepage    https://gitlab.com/almbrainlet/sadtorrent
// @description Inlines all Torrents into the gallery page, provides shortcut for the biggest file and adds eze info.json downloads
// @license     GPL-3.0-or-later
// @compatible  firefox
// @icon        data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAACXBIWXMAAAsSAAALEgHS3X78AAAC9klEQVR4Xu3cMW4bQRBEUdIQnPuSPpQvqVyJzBO4g0KbU4OnlNOzNb8+NqCW+3j4QwABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAIHbCDynA/3++et7WvPOz/98fY5nSPKdfv7pbBOfH9MGPr+bAAHu7nc8HQFGRHcvIMDd/Y6nI8CI6O4FBLi73/F0BBgR3b2AAHf3O56OACOiuxcQ4O5+x9MRYER09wIC3N3veDoCjIjuXkCAu/sdT0eAEdHdCz62jzf9P3r7+un+af70eYLp+q/9o+ch3AFSQ8rnCVBeYBqfACnB8nkClBeYxidASrB8ngDlBabxCZASLJ8nQHmBaXwCpATL5wlQXmAanwApwfJ5ApQXmMYnQEqwfJ4A5QWm8QmQEiyfJ0B5gWl8AqQEy+cJUF5gGp8AKcHyeQKUF5jGJ0BKsHyeAOUFpvEJkBIsn1//XcD2c/ET/3dff8qXfv763UD0Hkd3gLSB8nkClBeYxidASrB8ngDlBabxCZASLJ8nQHmBaXwCpATL5wlQXmAanwApwfJ5ApQXmMYnQEqwfJ4A5QWm8QmQEiyfJ0B5gWl8AqQEy+fXnweY3nO3ze/d198+X7q/O0BKsHyeAOUFpvEJkBIsnydAeYFpfAKkBMvnCVBeYBqfACnB8nkClBeYxidASrB8ngDlBabxCZASLJ8nQHmBaXwCpATL5wlQXmAanwApwfJ5ApQXmMYnQEqwfJ4A5QWm8QmQEiyfJ0B5gWl8AqQEy+cJUF5gGp8AKcHy+fXfBaTv6Zv4bj/3/8r//FeG9D190/m2P3cH2CZ8+P4EOLyg7XgE2CZ8+P4EOLyg7XgE2CZ8+P4EOLyg7XgE2CZ8+P7r3wMcfv443vQ9QXyB5Q3cAZYBn749AU5vaDkfAZYBn749AU5vaDkfAZYBn749AU5vaDkfAZYB2x4BBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEPjfBP4C9+srWG8VAzkAAAAASUVORK5CYII=
// @updateURL   https://gitlab.com/almbrainlet/sadtorrent/raw/master/sadtorrent.user.js
// @downloadURL https://gitlab.com/almbrainlet/sadtorrent/raw/master/sadtorrent.user.js
// @match       https://e-hentai.org/g/*
// @match       https://exhentai.org/g/*
// @grant       none
// ==/UserScript==

// Configuration

const INLINE_TORRENTS = true
const INFO_DOWNLOADS = true

// Configuration end

// TODO: Add info.json to shortcut. use a delay with setTimeout?

let tags
if (INFO_DOWNLOADS) {
  tags = getTags()

  const filename = document.getElementById('gn').innerText + '.info.json'
  const insNode = document.getElementById('gd5').childNodes[2]
  const newNode = document.createElement('p')
  const abhref = document.createElement('a')
  const empbTex = document.createTextNode(' ')

  const imgNode = document.createElement('img')
  const empTex = document.createTextNode(' ')
  const ahref = document.createElement('a')

  imgNode.setAttribute('src', 'https://ehgt.org/g/mr.gif')
  ahref.innerText = '[*.]'
  downJson(tags, filename, ahref)
  abhref.innerText = 'info.json Download'
  downJson(tags, 'info.json', abhref)
  newNode.setAttribute('class', 'g2')

  newNode.appendChild(imgNode)
  newNode.appendChild(empTex)
  newNode.appendChild(ahref)
  newNode.appendChild(empbTex)
  newNode.appendChild(abhref)

  insNode.insertAdjacentElement('afterend', newNode)
}

if (INLINE_TORRENTS) {
  fetchURI(getTorrentLink()).then(response => injectTorrents(response))
}

function getTorrentLink () {
  const torText = document.getElementById('gd5').childNodes[2].childNodes[2].attributes['onclick'].textContent
  const patt = /(https:.+)'/i
  const result = torText.match(patt)

  return result[1]
}

async function fetchURI (uri) {
  try {
    const response = await fetch(uri)
    const html = await response.text()
    const parser = new DOMParser()

    return parser.parseFromString(html, 'text/html')
  } catch (err) {
    console.log('Failed to fetch page: ', err)
  }
}

function arrayMaxIndex (sizes, seeds) {
  let maxS = sizes[0]
  let maxSs = seeds[0]
  let maxIndexS = 0
  let maxIndexSs = 0
  const lengthS = sizes.length
  let iS = 0

  for (; iS < lengthS; iS++) {
    if (sizes[iS] > maxS) {
      maxIndexS = iS
      maxS = sizes[iS]
    }
    if (seeds[iS] > maxSs) {
      maxIndexSs = iS
      maxSs = seeds[iS]
    }
  }

  // Use the best seeded one only if both have the same size
  if (maxIndexS !== maxIndexSs && sizes[maxIndexS] === sizes[maxIndexSs]) {
    maxIndexS = maxIndexSs
  }

  return maxIndexS
}

function injectTorrents (html) {
  const torrentNodes = html.getElementById('torrentinfo').firstElementChild.querySelectorAll('form')

  if (torrentNodes.length === 0) {
    return
  }
  let sizes = []
  let seeds = []

  // Padding needed for correct sorting
  const padding = new Intl.NumberFormat('en', {
    minimumIntegerDigits: 20,
    minimumFractionDigits: 2,
    useGrouping: false
  })

  torrentNodes.forEach(function (item, index) {
    // get Size:/Seeders: row
    const sizeRow = item.querySelectorAll('td span')[1].nextSibling.textContent.trim()
    let size = Number(sizeRow.split(' ')[0])
    const sizeType = sizeRow.split(' ')[1]

    // We want to have everything in MB
    if (sizeType === 'GB') {
      size = size * 1024
    }
    if (sizeType === 'KB') {
      size = size / 1024
    }

    sizes[index] = padding.format(size)

    const seedersRow = item.querySelectorAll('td span')[2].nextSibling.textContent.trim()
    const seeders = Number(seedersRow.split(' ')[0])

    seeds[index] = padding.format(seeders)
  })

  const maxSize = arrayMaxIndex(sizes, seeds)

  const insNode = document.getElementsByClassName('gm')[0]
  const newNode = document.createElement('div')

  newNode.setAttribute('class', 'gm torrent-info')
  newNode.setAttribute('style', 'background: inherit;')

  torrentNodes.forEach(function (item, index) {
    const torNode = item.querySelectorAll('tr')[2].firstElementChild.firstElementChild

    if (index === maxSize) {
      const downLabel = item.querySelectorAll('td span')[1].parentNode.nextElementSibling
      const downAnchor = document.createElement('a')

      downLabel.setAttribute('style', 'text-align: center; font-weight: bold; color: darkred; font-size: larger; text-decoration: underline;')
      downAnchor.textContent = 'DOWNLOAD WITH CTRL+SPACE'
      downAnchor.href = '#'
      downAnchor.onclick = function (event) {
        event.preventDefault()
        torNode.click()
      }
      downLabel.appendChild(downAnchor)

      // https://keycode.info/
      window.addEventListener('keydown', function (event) {
        if (!event.repeat && event.ctrlKey && event.code === 'Space') {
          torNode.click()
          /*
          if (INFO_DOWNLOADS) {
            // TODO: Fix shortcut, how to trigger both?
            // Figure out how to trigger both
            // Only the last one works
            setTimeout(() => {
              document.getElementsByClassName('downjson')[0].click()
            }, 5)
          }
          */
        }
      }, true)
    }

    if (INFO_DOWNLOADS) {
      const jsonName = torNode.textContent + '.info.json'
      const newtd = document.createElement('td')
      const newa = document.createElement('a')

      torNode.parentElement.setAttribute('colspan', 4)
      newa.innerText = '[torrentname]info.json'

      if (index === maxSize) {
        newa.setAttribute('class', 'downjson')
      }

      downJson(tags, jsonName, newa)

      newtd.setAttribute('colspan', 1)
      newtd.appendChild(newa)

      torNode.parentElement.insertAdjacentElement('afterend', newtd)
    }

    newNode.appendChild(item)
  })
  insNode.insertAdjacentElement('afterend', newNode)
}

function getTags () {
  let jsonInfo = {
    gallery_info: {
      title: null,
      title_original: null,

      category: null,
      tags: {},

      language: null,
      translated: null,

      favorite_category: null,

      upload_date: [
        null,
        null,
        null,
        null,
        null,
        0
      ],

      source: {
        site: null,
        gid: null,
        token: null,
        parent_gallery: null,
        newer_versions: []
      }
    }
  }

  const jsonInfoGal = jsonInfo['gallery_info']
  const rows = document.getElementById('gdd').querySelectorAll('tr td')
  let language
  let posted

  jsonInfoGal['title'] = document.getElementById('gn').textContent
  jsonInfoGal['title_original'] = document.getElementById('gj').textContent
  jsonInfoGal['category'] = document.getElementById('gdc').textContent.toLowerCase()

  rows.forEach(function (item) {
    if (item.textContent === 'Language:') {
      language = item.nextElementSibling.textContent
    }

    if (item.textContent === 'Posted:') {
      posted = item.nextElementSibling.textContent
    }

    if (item.textContent === 'Parent:' && item.nextElementSibling.textContent !== 'None') {
      const parent = item.nextElementSibling.firstElementChild.href.split('/')
      jsonInfoGal['source']['parent_gallery'] = { 'gid': parseInt(parent[4]), 'token': parent[5] }
    }
  })

  jsonInfoGal['language'] = language.split(' ')[0].trim()

  if (language.split(' ')[1].trim() === 'TR') {
    jsonInfoGal['translated'] = true
  } else {
    jsonInfoGal['translated'] = false
  }

  jsonInfoGal['upload_date'][0] = parseInt(posted.split(' ')[0].split('-')[0])
  jsonInfoGal['upload_date'][1] = parseInt(posted.split(' ')[0].split('-')[1])
  jsonInfoGal['upload_date'][2] = parseInt(posted.split(' ')[0].split('-')[2])
  jsonInfoGal['upload_date'][3] = parseInt(posted.split(' ')[1].split(':')[0])
  jsonInfoGal['upload_date'][4] = parseInt(posted.split(' ')[1].split(':')[1])

  const taglist = document.getElementById('taglist').querySelectorAll('tr')
  taglist.forEach(function (item) {
    jsonInfoGal['tags'][item.children[0].textContent.split(':')[0]] = item.children[1].innerText.split('\n')
  })

  const docuri = document.documentURI.split('/')
  jsonInfoGal['source']['site'] = docuri[2].split('.')[0]
  jsonInfoGal['source']['gid'] = parseInt(docuri[4])
  jsonInfoGal['source']['token'] = docuri[5]

  let newVers = document.getElementById('gnd')

  if (newVers) {
    newVers = newVers.querySelectorAll('a')
    newVers.forEach(function (item, index) {
      const gidToken = item.href.split('/')
      const curDate = item.nextSibling.textContent
      const patt = /(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d)/i
      const dime = curDate.match(patt)
      const newDate = new Date()
      newDate.setUTCFullYear(dime[1])
      newDate.setUTCMonth(dime[2] - 1)
      newDate.setUTCDate(dime[3])
      newDate.setUTCHours(dime[4])
      newDate.setUTCMinutes(dime[5])
      newDate.setUTCSeconds(0)
      newDate.setUTCMilliseconds(0)
      jsonInfoGal['source']['newer_versions'][index] = {
        'gallery': {
          'gid': parseInt(gidToken[4]),
          'token': gidToken[5]
        },
        'name': item.textContent,
        'date_uploaded': newDate.valueOf()
      }
    })
  }

  const fav = document.getElementById('fav').childNodes

  if (fav.length > 0) {
    jsonInfoGal['favorite_category'] = {
      'id': parseInt(fav[0].title.split(' ')[1]),
      'title': fav[0].title
    }
  }

  return jsonInfo
}

function downJson (text, fileName, node) {
  const type = fileName.split('.').pop()
  const json = JSON.stringify(text, null, '  ')
  const name = filenameIt(fileName)

  node.href = '#'
  // The hard way
  node.onclick = function () {
    // IE 11
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      const blob = new Blob([json], { type: `text/${type === 'txt' ? 'plain' : type}` })
      window.navigator.msSaveOrOpenBlob(blob, name)
    } else { // Others
      const file = new File([json], name, { type: `text/${type === 'txt' ? 'plain' : type}` })
      const exportUrl = URL.createObjectURL(file)
      this.href = exportUrl
      this.download = name
      setTimeout(() => {
        URL.revokeObjectURL(exportUrl)
        this.href = '#'
      }, 1)
    }
  }
  /*
  // The easy way.
  node.onclick = function () {
    this.href = URL.createObjectURL(new Blob([json], {
      type: `text/${type === 'txt' ? 'plain' : type}`
    }))
    this.download = name
  }
  */
}

// Taken from eze script
function filenameIt (fileName) {
  let filenameNormalize = function (name) {
    // eslint-disable-next-line no-control-regex
    name = fileName.replace(/[\x00-\x1F]+/g, '')
    name = name.replace(rePattern, function (m) {
      return charMap[m]
    })

    return name
  }

  let charMap = {
    '"': '\u201d',
    '<': '\uff1c',
    '>': '\uff1e',
    ':': '\uff1a',
    '|': '\uff5c',
    '?': '\uff1f',
    '*': '\uff0a',
    '/': '\uff0f',
    '\\': '\uff3c'
  }
  let rePattern = '['
  let k

  for (k in charMap) {
    rePattern += k.replace(/([^\w\s])/g, '\\$1')
  }

  rePattern += ']'
  rePattern = new RegExp(rePattern, 'g')

  return filenameNormalize()
}
