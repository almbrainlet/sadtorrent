# Sadpanda Torrent Inline + info.json userscript

* Inlines all Torrents into the gallery page
* Provides shortcut for the biggest file
* Adds eze info.json download buttons with custom filename

# Installation

* Install a userscript manager for your browser
* [Click Here](https://gitlab.com/almbrainlet/sadtorrent/raw/master/sadtorrent.user.js)

# Configuration

* INLINE_TORRENTS = `true`
* * Enables torrent inlining
* INFO_DOWNLOADS = `true`
* * Enables info.json Buttons

![alt text](previewGalInfo.png "Gallery Info")
![alt text](previewTorInfo.png "Torrent Info")
